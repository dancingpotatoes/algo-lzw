

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * program to find compression ratio using LZW compression
 */
public class Main {

    public static void main(String[] args) throws IOException {

        long start = System.currentTimeMillis();
        int binaryCode = 0;
        int codeWordLen = 8;
        Trie trieTree = new Trie();
        String inputFileName = args[0];
        FileReader reader = new FileReader(inputFileName);
        BufferedReader in = new BufferedReader(reader);
        String s, nextChar;
        int compressedBits = 0;

        for(int i = 0; i < 128; i++){
            s = Character.toString((char) i);

            trieTree.insert(s);
            binaryCode++;
        }

        int temp;
        s = "";
        int fileLength = 0;

        while ((temp = in.read()) != -1){

            fileLength++;


            nextChar = Character.toString((char) temp);
            if (trieTree.search(s + nextChar)){
                s = s + nextChar;
            } else {

                compressedBits += codeWordLen;

                if (Integer.toBinaryString(binaryCode).length() > codeWordLen){
                    codeWordLen++;
                }


                trieTree.insert(s + nextChar);

                binaryCode++;

                s = nextChar;
            }
        }

        if (Integer.toBinaryString(binaryCode).length() > codeWordLen){
            codeWordLen++;
        }
        compressedBits += codeWordLen;

        in.close();
        reader.close();

        // read a line at a time to enable newlines to be detected and included in the compression


        String outputFileName = args[1];
        FileWriter writer = new FileWriter(outputFileName);
        writer.write("Input file " + inputFileName + "  LZW algorithm Trie Implementation\n\n");

        // System.out.println("compressed bits: " + compressedBits + "\n");
        writer.write("Original file length in bits = " + fileLength*8 + "\n");
        writer.write("Compressed file length in bits = " + compressedBits + "\n");
        writer.write("Compression ratio = " + (float)compressedBits/(float)(fileLength*8) );


        // write out the results here

        long end = System.currentTimeMillis();
        writer.write("\nElapsed time: " + (end - start) + " milliseconds\n");
        writer.close();
    }

}


